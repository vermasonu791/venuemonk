import React from 'react';

const footer = () =>{

    return (
    <footer>
        <div className="footer" id="footer">
            <div className="container">
                <div className="row">
                <div className="col-md-4">
                    <h6>Subscribe To Us</h6>
                        <form action="">
                            <div className="form-group">
                                <label htmlFor="name">Name</label>
                                <input type="text" className="form-control" id="name" placeholder="Name" />
                            </div>
                            <div className="form-group">
                                <label htmlFor="email">Email address:</label>
                                <input type="email" className="form-control" id="email" placeholder="Email" />
                            </div>
                            <button type="submit" className="btn btn-primary">Subscribe</button>
                        </form>
                    </div>
                    <div className="col-md-4 footer-v">
                        <h6 style={{textAlign:'center'}}>VenueMonk</h6>
                        <ul className="">
                            <li><a href="#">About us</a></li>
                            <li><a href="#">Cobtact us</a></li>
                            <li><a href="#">Careers</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Term of Use</a></li>
                        </ul>
                    </div>
                    <div className="col-md-4 footer-v">
                        <h6 style={{textAlign:'center'}}>Venues</h6>
                        <ul className="">
                            <li><a href="#">List Your Business</a></li>
                            <li><a href="#">Why Venue Monk</a></li>
                            <li><a href="#">FAQs</a></li>
                        </ul>
                        <div className="middle" style={{textAlign:'center'}}>
                            <h6 style={{textAlign:'center'}}>Follow Us</h6>       
                            <a className="btn" href="#">
                                <i className="fab fa-facebook-f"></i>
                            </a>
                            <a className="btn" href="#">
                                <i className="fab fa-twitter"></i>
                            </a>
                            <a className="btn" href="#">
                                <i className="fab fa-google"></i>
                            </a>
                            <a className="btn" href="#">
                                <i className="fab fa-instagram"></i>
                            </a>
                            <a className="btn" href="#">
                                <i className="fab fa-youtube"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <hr style={{backgroundColor:'#1abc9c !important'}} />
                <br />
                <p style={{fontSize:'12px'}}>Copyright © 2018 PurplePatch Online Services | All Rights Reserved</p> 
            </div>
        </div>
    </footer>
    );
}


export default footer;