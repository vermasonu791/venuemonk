import React from 'react';

const image1 = require('./1.jpg');

const home = () =>{
    return (
        <div className="wrap">
        <div className="top">
            <nav className="navbar navbar-expand-md  navbar-dark sticky-top">
                <a className="navbar-brand" href="#"><img src="https://venuemonk-images.s3.amazonaws.com/images/logo-v3.png" alt="" /></a>
                <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="collapsibleNavbar">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Venues</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Collection</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#">Blog</a>
                        </li>  
                    </ul>
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <a className="nav-link" href="#">Login&nbsp;<i className="fas fa-sign-in-alt" style={{fontSize:'11px'}}></i></a>
                        </li>
                        <li className="nav-item">
                            <a className="nav-link" href="#">Register&nbsp;<i className="fas fa-registered" style={{fontSize:'11px'}}></i></a>
                        </li>  
                    </ul>
                </div>  
            </nav>
            <div className="text">
                <marquee behavior="" direction="right"><p>LET US PLAN YOUR EVENT!</p></marquee>
                    
                </div>
                <div className="info">
                 <div className="row">
                     <div className="col-md-6">
                       <div id="box1">
                            <img src={image1} className="img-fluid" alt="" />
                       </div>
                     </div>
                     <div className="col-md-6">
                         <div id="box2">
                            Expand your event planning service with VenueMonk
                            <hr />
                            <div className="row" style={{margin:'0px auto'}}>
                                <div className="col-lg-4 col-xs-6 col-sm-6 ">
                                    <a className="btn" href="">
                                            <img src="https://venuemonk-images.s3.ap-south-1.amazonaws.com/images/sliderform/birthday-party.png" alt="" />
                                            <h6>Birthday-Party</h6>
                                    </a>
                                    
                                </div>
                                <div className="col-lg-4 col-xs-6 col-sm-6">
                                        <a className="btn" href="">
                                            <img src="https://venuemonk-images.s3.ap-south-1.amazonaws.com/images/sliderform/team-party.png" alt="" />
                                            <h6>Team-party</h6>
                                        </a>
                                </div>
                                <div className="col-lg-4 col-xs-6 col-sm-6">
                                    <a className="btn" href="">
                                        <img src="https://venuemonk-images.s3.ap-south-1.amazonaws.com/images/sliderform/wedding.png" alt="" />
                                        <h6>Wedding</h6>
                                    </a>
                                </div>
                                <div className="col-lg-4 col-xs-6 col-sm-6">
                                    <a className="btn" href="">
                                        <img src="https://venuemonk-images.s3.ap-south-1.amazonaws.com/images/sliderform/cocktail-party.png" alt="" />                                                    
                                        <h6>Cocktail-party</h6>
                                    </a>
                                    </div>
                                    <div className="col-lg-4 col-xs-6 col-sm-6">
                                        <a className="btn" href="">
                                            <img src="https://venuemonk-images.s3.ap-south-1.amazonaws.com/images/sliderform/conference.png" alt="" />                                                        <h6>Corporate-events</h6>
                                        </a>
                                    </div>
                                    <div className="col-lg-4 col-xs-6 col-sm-6">
                                        <a className="btn" href="">
                                            <img src="https://venuemonk-images.s3.ap-south-1.amazonaws.com/images/sliderform/engagement.png" alt="" />                                                        <h6>Corporate-events</h6>
                                        </a>
                                    </div>
                            </div>
                            <div className="book-now" style={{padding:'10px'}}>
                                <button className="btn btn-primary">Book-Now</button>
                            </div>
                         </div>
                    </div>
                 </div>
                </div>
            </div>
            <div className="info-2">
                <div className="container">
                    <div className="row">
                            <div className="col-md-4">
                                <div className="box-1">
                                    <div style={{textAlign:'center'}}>
                                        <a className="" href="" style={{textDecoration:'none'}}>
                                            <img src="https://venuemonk-images.s3.amazonaws.com/images/save-money.png" alt="" />                                                    
                                            <h6>Grow Business</h6>
                                        </a>
                                    </div>
                                    <ul>
                                        <li>Minimum Business Guarantee</li>
                                        <li>No need for pushing for leads. Take advantage of our experience.</li>
                                        <li>We give our partners volume</li>
                                        <li>We get you verified leads and more!</li>
                                    </ul>
                                    <div className="book-now" style={{paddingTop:'10px',textAlign:'center'}}>
                                        <button className="btn btn-primary">Know-More</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="box-1">
                                    <div style={{textAlign:'center'}}>
                                        <a className="" href="" style={{textDecoration:'none'}}>
                                            <img src="https://venuemonk-images.s3.amazonaws.com/images/save-time.png" alt="" />                                                    
                                            <h6>Save Time</h6>
                                        </a>
                                    </div>
                                    <ul>
                                        <li>Minimum Business Guarantee</li>
                                        <li>No need for pushing for leads. Take advantage of our experience.</li>
                                        <li>We give our partners volume</li>
                                        <li>We get you verified leads and more!</li>
                                    </ul>
                                    <div className="book-now" style={{paddingTop:'10px',textAlign:'center'}}>
                                        <button className="btn btn-primary">Know-More</button>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="box-1">
                                    <div style={{textAlign:'center'}}>
                                        <a className="" href="" style={{textDecoration:'none'}}>
                                            <img src="https://venuemonk-images.s3.amazonaws.com/images/verified-listings.png" alt="" />                                                    
                                            <h6>Verfied Listings</h6>
                                        </a>
                                    </div>
                                    <ul>
                                        <li>Minimum Business Guarantee</li>
                                        <li>No need for pushing for leads. Take advantage of our experience.</li>
                                        <li>We give our partners volume</li>
                                        <li>We get you verified leads and more!</li>
                                    </ul>
                                    <div className="book-now" style={{paddingTop:'10px',textAlign:'center'}}>
                                        <button className="btn btn-primary">Know-More</button>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                <div className="container" style={{paddingBottom:'30px'}}>
                    <br />
                    <h3 style={{textAlign:'center'}}>Why Choose VenueMonk?</h3>
                    <div className="row">
                        <div className="col-md-4">
                            <div style={{textAlign:'center'}}>
                                <img src="https://venuemonk-images.s3.amazonaws.com/images/save-money.png" alt="" />                                                    
                                <h5>Save Money</h5>
                                <h6>Lowest Price Guaranteed</h6>
                                <p>No need for 100s of phone calls and long negotiations. Take advantage of our experience.</p>
                            </div>
                        </div>
                    <div className="col-md-4">
                        <div style={{textAlign:'center'}}>
                            <img src="https://venuemonk-images.s3.amazonaws.com/images/verified-listings.png" alt="" />                                                    
                            <h5>Verified Listings</h5>
                            <h6>Dependable & Accurate</h6>
                            <p>What you see is what you get! We use a 25 data point system to collect accurate information.</p>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div style={{textAlign:'center'}}>
                            <img src="https://venuemonk-images.s3.amazonaws.com/images/save-time.png" alt="" />                                                    
                            <h5>Hassle Free Booking</h5>
                            <h6>Convenience</h6>
                            <p>No more running around to find the right venue. Just create your event.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="container">
                <hr style={{color:'#34ace0'}} />
                <h5 style={{textAlign:'center'}}>Get Started Now</h5>
                <div className="row">
                    <div className="col-md-5 text-center">
                        <div className="book-now" >
                            <button className="btn btn-primary">Send-Enquiry</button>
                        </div>
                    </div>
                    <div className="col-md-2">
                        <p style={{textAlign:'center',fontSize:'16px'}}>Or</p>
                    </div>  
                    <div className="col-md-5 text-center">
                        <div className="book-now" >
                            <button className="btn btn-primary">Call-Us</button>
                        </div>
                    </div>
                </div>
                <br /><br />
            </div>
            <div id="image1">
                <div className="container">
                    <div id="wedding-info">
                        <div id="text5">
                            <h6 style={{textAlign:'center', fontSize:'20px'}}>Let Us Wow You!</h6>
                            <br />
                            <p style={{textAlign:'center'}}>Ready to take it a step further? Let’s start and Book your Events.</p>
                            <br />
                            <div className="book-now" style={{textAlign:'center'}}>
                                <button className="btn btn-primary">Book-Know</button>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
            <br />
            <div id="faq">
                <div className="container">
                    <br />
                    <h6 style={{textAlign:'center',fontSize:'20px'}}>Frequently Asked Questions</h6>
                    <p  style={{textAlign:'center'}}>Top Queries</p>
                    <hr />
                    <div className="row">
                        <div className="col-md-12">
                            <p style={{fontWeight:'bold',fontSize:'15px'}}>Q. What is VenueMonk?</p>
                            <p style={{fontSize:'15px'}}>A. VenueMonk is a technology enabled event planning service. You can explore over a thousand listed venues and vendors for planning your event. Not only do we provide detailed, comprehensive and current information on venues and vendors, we also help you book these services at the lowest market price with just a or a call to 971-777-9251.</p>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-12">
                            <p style={{fontWeight:'bold',fontSize:'15px'}}>Q. Are you an event planner?</p>
                            <p style={{fontSize:'15px'}}>A. No, we do not plan but empower you to grow your event services with use of technology and comprehensive software solutions.</p>
                        </div>
                    </div>
                                <div className="row">
                                        <div className="col-md-12">
                                            <p style={{fontWeight:'bold',fontSize:'15px'}}>Q. Do you charge for the partner Dashboard</p>
                                            <p style={{fontSize:'15px'}}>A. No, we do not charge anything from our partners extra for the partner dashboard. All costs are included when we sign you up and the contract.</p>
                                        </div>
                                </div>
                                <div className="row">
                                        <div className="col-md-12">
                                            <p style={{fontWeight:'bold',fontSize:'15px'}}>Q. How do you Earn?</p>
                                            <p style={{fontSize:'15px'}}>A. Venues and Vendors are required to make an agreement with us for mutual business growth.</p>
                                        </div>
                                </div>
                                <div className="row">
                                        <div className="col-md-12">
                                            <p style={{fontWeight:'bold',fontSize:'15px'}}>Q. Once the venue is finalized, where is the payment done?</p>
                                            <p style={{fontSize:'15px'}}>A. Customer can pay the booking amount online to us or directly to the venue, whichever is more comfortable for him/her. On paying the booking amount to us or the venue, we confirm the booking.</p>
                                        </div>
                                </div>
                        </div>
                </div>
    </div>
    );

}

export default home;