import React, { Component } from 'react';
import './App.css';
import Home from './Home/Home.js';
import Footer from './Footer/Footer.js';

class App extends Component {
  render() {
    return (
      <div className="App">
     
     <Home />
     <Footer />
      </div>
    );
  }
}

export default App;
